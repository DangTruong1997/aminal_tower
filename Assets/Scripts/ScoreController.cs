﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    private Animator anim;

    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    public void Play()
    {
        GameManager.Instance.gameController.OnClick_LoadSceneGamePlay();
    }

    public void OnClick_Play()
    {
        AudioManager.Instance.audioButtonPlay.Play();
        anim.Play("ScoreOff");
    }
}
