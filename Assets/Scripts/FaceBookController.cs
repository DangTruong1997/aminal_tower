﻿using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceBookController : MonoBehaviour
{

    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init();
        }
        else
            FB.ActivateApp();
    }

    public void Share()
    {
        FB.ShareLink(
            new Uri("http://www.test.com/"),
                "test title",
                "test description",
                new Uri("https://cdn.ugame.vn/millionaire_game_question/thumbnail/2017/08/05/34d7bb34cb095892c793d2313ecd5134.jpg"),
            //contentTitle: "Rebisco",
            //contentURL: new System.Uri("http://n3k.ca"),
            //contentDescription: "Lets have a great adventure.",
            callback: OnShare
            );
    }

    private void OnShare(IShareResult result)
    {
        if(result.Cancelled||!string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Share err : " + result.Error);
        }
        else if(!string.IsNullOrEmpty(result.PostId))
        {
            Debug.Log(result.PostId);
        }
        else
        {
            Debug.Log("share succeed");
        }

    }


}
