﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager
{
    #region create GameManager
    private static GameManager instance;

    private GameManager()
    {

    }

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameManager();
            }

            return instance;
        }
    }
    #endregion

    public GameController gameController;
    
    public PosRotateController posRotateController;

    public CameraController cameraController;

}


