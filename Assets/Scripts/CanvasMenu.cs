﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasMenu : MonoBehaviour
{
    private Animator anim;

    public float ratioScaleGamePlay;

    [SerializeField]
    private GameObject audioBG;
    private void Awake()
    {
        Camera.main.aspect = 1080f / 1920f;
        ratioScaleGamePlay = Screen.width / 1080f;
    }
    public void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        if(AudioManager.Instance.audioBGController == null)
        {
            Instantiate(audioBG);
        }
    }

    public void Play()
    {
        SceneManager.LoadScene(ConfigScene.SceneGanePlay);
    }

    public void OnClick_Play()
    {
        AudioManager.Instance.audioButtonPlay.Play();
        anim.Play("menuOff");
    }
}
