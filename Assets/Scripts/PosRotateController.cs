﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PosRotateController : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    public List<GameObject> arrEnemy = new List<GameObject>();

    public GameObject child;

    private bool checkCreateAnimal = false;

    private bool checkClickChange = false;

    private bool checkClick = false;

    public GameObject particle;

    private ParticleSystem particleSystem;

    public List<GameObject> arrEnemyInstance = new List<GameObject>();


    Vector3 mousePosition;

    private void Awake()
    {
        GameManager.Instance.posRotateController = this;
    }

    private void Start()
    {
        particleSystem = particle.GetComponent<ParticleSystem>();
        CreateEnemy();
    }

    public void MoveCamera()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y + 1);
    }

    public void CreateEnemy()
    {
        if (!checkCreateAnimal)
        {
            
            StartCoroutine(CreatorEnemyWait());
        }
    }

    IEnumerator CreatorEnemyWait()
    {
        yield return new WaitForSeconds(0.8f);
        if (!GameManager.Instance.gameController.checkDie)
        {
            AudioManager.Instance.audioAppearAnimal.Play();
            particle.SetActive(true);
            particleSystem.Stop();
            particleSystem.Play();
            int idEnemy = Random.Range(0, arrEnemy.Count);
            child = Instantiate(arrEnemy[idEnemy], transform.position, arrEnemy[idEnemy].transform.rotation);
            arrEnemyInstance.Add(child);
            child.transform.parent = gameObject.transform;
            child.transform.localScale = Vector3.zero;
            child.transform.DOScale(Vector3.one, 0.7f);
            checkCreateAnimal = true;
        }

    }

    public void OnClick_Rotate()
    {
        if (checkCreateAnimal && !GameManager.Instance.gameController.checkGameOver && !checkClickChange)
        {
            AudioManager.Instance.audioRotate.Play();
            gameObject.transform.DORotate(transform.eulerAngles + Quaternion.AngleAxis(-30, new Vector3(0, 0, 1)).eulerAngles, 0.5f);
        }
    }

    public void OnClick_Change()
    {
        if (checkCreateAnimal && !checkClickChange)
        {
            AudioManager.Instance.audioRotate.Play();
            Destroy(child);
            checkCreateAnimal = false;
            arrEnemyInstance.RemoveAt(arrEnemyInstance.Count - 1);
            CreateEnemy();
            GameManager.Instance.gameController.OnClick_Change();
        }
    }

    private void Update()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (!MyUntil.IsPointerOverUIObject())
            {
                AudioManager.Instance.audioDrop.PlayPop();
                checkClickChange = true;
            }
            else
            {
                checkClickChange = false;
            }
        }

        if (Input.GetMouseButton(0) )
        {
            if (checkCreateAnimal && !GameManager.Instance.gameController.checkGameOver && checkClickChange)
            {
                child.transform.position = new Vector2(mousePosition.x, child.transform.position.y);
            }

        }

        if (Input.GetMouseButtonUp(0))
        {
            if (checkCreateAnimal && !GameManager.Instance.gameController.checkGameOver && checkClickChange)
            {
                AudioManager.Instance.audioDrop.PlayDrop();
                child.GetComponent<EnemyController>().ChangeEnemy();
                transform.rotation = Quaternion.identity;
                GameManager.Instance.gameController.score++;
                checkCreateAnimal = false;
                checkClickChange = false;
            }
        }
    }

    public void OnClickPlay()
    {
        for(int i=0; i<arrEnemyInstance.Count;i++)
        {
            arrEnemyInstance[i].transform.DOMove(target.position,0.45f);
            arrEnemyInstance[i].transform.DOScale(Vector3.zero, 0.45f);
            arrEnemyInstance[i].transform.DORotate(new Vector3(0,0,1000), 0.45f);
        }
    }

    //public void RotateEnemyDie(GameObject gameObject)
    //{
    //    gameObject.transform.Rotate(0,0,transform.rotation.z + 15);
    //}

}
