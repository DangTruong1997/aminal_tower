﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Text txtScore;

    [SerializeField]
    private GameObject uiGamePlay;

    [SerializeField]
    private GameObject uiGameOver;

    public int score;

    public bool checkGameOver = false;

    [SerializeField]
    private Text txtScoreGameOver;

    [SerializeField]
    private Text txtHeighScore;

    [SerializeField]
    private GameObject btnChange;

    public float ratioScaleGamePlay;

    public bool checkDie;



    private void Awake()
    {
        GameManager.Instance.gameController = this;
        Camera.main.aspect = 1080f / 1920f;
        ratioScaleGamePlay = Screen.height / 1920f;
    }

    public static bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }


    public void OnClick_LoadSceneMenu()
    {
        //;
        AudioManager.Instance.audioRotate.Play();
        SceneManager.LoadScene(ConfigScene.SceneMenu);
    }

    public void OnClick_LoadSceneGamePlay()
    {
        SceneManager.LoadScene(ConfigScene.SceneGanePlay);
    }

    public void GameOver()
    {
        AudioManager.Instance.audioScore.Play();
        SetHighScore();
        checkDie = true;
        uiGamePlay.SetActive(false);
        uiGameOver.SetActive(true);
        checkGameOver = true;
    }

    public void SetHighScore()
    {
        int a = PlayerPrefs.GetInt("score", 0);
        if(score>=a)
        {
            PlayerPrefs.SetInt("score", score);
            PlayerPrefs.Save();
        }
    }

    public void OnClick_Change()
    {
        btnChange.SetActive(false);
    }

    public int GetHighScore()
    {
        int a = PlayerPrefs.GetInt("score", 0);
        return a;
    }

    private void Update()
    {
        txtScore.text = GameManager.Instance.gameController.score.ToString();
        txtScoreGameOver.text = GameManager.Instance.gameController.score.ToString();
        txtHeighScore.text = GetHighScore().ToString();
    }
}
