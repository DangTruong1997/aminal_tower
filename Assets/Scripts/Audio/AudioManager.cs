﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager
{
    #region create GameManager
    private static AudioManager instance;

    private AudioManager()
    {

    }

    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new AudioManager();
            }

            return instance;
        }
    }
    #endregion

    public AudioButtonPlay audioButtonPlay;

    public AudioAppearAnimal audioAppearAnimal;

    public AudioDrop audioDrop;

    public AudioScore audioScore;

    public AudioRotate audioRotate;

    public AudioBGController audioBGController;
}
