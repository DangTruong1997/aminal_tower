﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioDrop : MonoBehaviour
{
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip drop;

    [SerializeField]
    private AudioClip pop;

    private void Awake()
    {
        AudioManager.Instance.audioDrop = this;
    }

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void PlayPop()
    {
        audioSource.clip = pop;
        audioSource.Play();
    }

    public void PlayDrop()
    {
        audioSource.clip = drop;
        audioSource.Play();
    }

}
