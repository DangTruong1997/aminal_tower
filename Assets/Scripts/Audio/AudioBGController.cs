﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBGController : MonoBehaviour
{
    private void Awake()
    {
        if(AudioManager.Instance.audioBGController == null)
        {
            AudioManager.Instance.audioBGController = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
