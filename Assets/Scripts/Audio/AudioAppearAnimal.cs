﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioAppearAnimal : MonoBehaviour
{
    private AudioSource audioSource;

    private void Awake()
    {
        AudioManager.Instance.audioAppearAnimal = this;
    }

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void Play()
    {
        audioSource.Play();
    }
}
