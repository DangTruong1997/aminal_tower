﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScore : MonoBehaviour
{
    private AudioSource audioSource;

    private void Awake()
    {
        AudioManager.Instance.audioScore = this;
    }

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private bool checkPlay;

    public void Play()
    {
        if (!checkPlay)
        {
            audioSource.Play();
            checkPlay = true;
        }
    }
}
