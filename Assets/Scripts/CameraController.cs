﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public float constMove;

    private void Awake()
    {
        GameManager.Instance.cameraController = this;
    }
    public void MoveCamera()
    {
        GameManager.Instance.posRotateController.MoveCamera();
        constMove += 1;
        transform.DOMoveY(transform.position.y + 1, 2).OnComplete(()=>
        {
            GameManager.Instance.posRotateController.CreateEnemy();
        });
    }
}
