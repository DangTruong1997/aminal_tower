﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Rigidbody2D rigid;

    private void Start()
    {
        rigid = gameObject.GetComponent<Rigidbody2D>();
    }

    public void ChangeEnemy()
    {
        transform.parent = null;
        rigid.bodyType = RigidbodyType2D.Dynamic;
    }

    private void Update()
    {
        
        if (transform.position.y<-1.5f)
        {
            GameManager.Instance.gameController.GameOver();
        }

        if (GameManager.Instance.gameController.checkGameOver)
        {
            rigid.bodyType = RigidbodyType2D.Static;
        }
    }

    public bool checkEnter = false;

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(!checkEnter)
        {
            if (coll.gameObject.tag == ConfigTag.tag_bg || coll.gameObject.tag == ConfigTag.tag_animal)
            {
                //GameManager.Instance.posRotateController.CreateEnemy();
                if (coll.gameObject.transform.position.y >= GameManager.Instance.cameraController.constMove)
                {
                    Debug.Log("MoveCamera");
                    GameManager.Instance.cameraController.MoveCamera();
                    checkEnter = true;
                }
                else
                {
                    GameManager.Instance.posRotateController.CreateEnemy();
                    checkEnter = true;
                }
            }
        }
    }
}
